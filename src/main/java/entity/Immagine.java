package entity;

import lombok.*;

@Getter
public class Immagine {
    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode
    @ToString
    public static class ImmagineName{
        String nomeFile;
    }

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode
    @ToString
    public static class ImmagineCompleta{
        String nomeFile;
        String base64Immagine;
    }

}
