package it.sabbydrive.risorse;

import entity.Immagine;
import multipart.Multipart;
import org.jboss.resteasy.annotations.cache.NoCache;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.imageio.ImageIO;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Path("immagini")
public class ImmagineResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @NoCache
    public Response immagini() {
        File directoryPath = new File("/users/matteosabatino/desktop/fotoUpload");
        //List of all files and directories
        String immagini[] = directoryPath.list();
        List<Immagine.ImmagineName> immaginiObj = new ArrayList();
        for(int i = 0 ; i < immagini.length; i++){
            Immagine.ImmagineName immagine = Immagine.ImmagineName.builder().nomeFile(immagini[i]).build();
            immaginiObj.add(immagine);
        }
        return Response.status(Response.Status.OK).entity(immaginiObj).build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertImmagini(@MultipartForm Multipart img) {
        String path = "/users/matteosabatino/desktop/fotoUpload/";
        String fileName = img.fileName;
        try {
            Image image = ImageIO.read(img.image);
            ImageIO.write((RenderedImage) image, "jpg", new File(path + fileName));
            return Response.status(Response.Status.OK).build();
        } catch (IOException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}


