package multipart;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;

public class Multipart {

    @FormParam("image")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    public InputStream image;

    @FormParam("fileName")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    public String fileName;
}
